<%-- 
    Document   : register
    Created on : Jan 8, 2020, 12:58:07 AM
    Author     : monirulalom
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
        <link href="bootstrap.css" rel="stylesheet">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="text-center">User Registration</h1>
            <form action="register" method="POST" class="form-signin">
                <input type="text" name="email" placeholder="Email" class="form-control">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <input  class="btn btn-lg btn-primary btn-block" type="submit" value="Register">
            </form>
            <p class="text-center">Already have an account ?<br> <a href="/jsplogin" >Login here</a></p>
        </div>

    </body>
</html>
