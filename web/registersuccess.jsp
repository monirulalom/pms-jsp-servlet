<%-- 
    Document   : loginsuccess
    Created on : Jan 8, 2020, 1:38:00 AM
    Author     : monirulalom
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Successfully Registered!</title>
        <link href="bootstrap.css" rel="stylesheet">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="text-center">
                <h1 class="text-center">Successfully Registered!</h1> 
                <a href="projects?action=view">Home Page</a>

            </div>
        </div>
    </body>
</html>
