package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import dao.UserDao;

@WebServlet(name = "registerController", urlPatterns = {"/register"})
public class registerController extends HttpServlet {

    private UserDao userdao;

    public void init() {
        userdao = new UserDao();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = new User();
        user.setUsername(email);
        user.setPassword(password);

        try {
            if (userdao.register(user) != 0) {
                //HttpSession session = request.getSession();
                // session.setAttribute("username",username);
                response.sendRedirect("registersuccess.jsp");
            } else {
                HttpSession session = request.getSession();
                //session.setAttribute("user", username);
                //response.sendRedirect("login.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
