package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.ProjectDao;
import dao.UserDao;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import model.Project;

/**
 *
 * @author monirulalom
 */
public class ProjectController extends HttpServlet {

    private ProjectDao projectdao;
    private UserDao userdao;

    public ProjectController() {
        super();
        projectdao = new ProjectDao();
        userdao = new UserDao();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        int uid = (Integer) session.getAttribute("user_id");
        String action = request.getParameter("action");
        String role = (String) session.getAttribute("user_role");
        if (action.equalsIgnoreCase("view")) {

            try {

                //  
                if (role.equalsIgnoreCase("developer")) {
                    request.setAttribute("projects", projectdao.getProjectListByUser(uid));
                } else {
                    request.setAttribute("projects", projectdao.getProjectList());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            RequestDispatcher view = request.getRequestDispatcher("project/projects.jsp");
            view.forward(request, response);

        }
        if (action.equalsIgnoreCase("add")) {
            try {

                request.setAttribute("users", userdao.getUserlist());

            } catch (Exception e) {
                e.printStackTrace();
            }

            RequestDispatcher view = request.getRequestDispatcher("project/project.jsp");
            view.forward(request, response);
        }
        if (action.equalsIgnoreCase("delete")) {
            int id = Integer.parseInt(request.getParameter("id"));

            projectdao.removeProject(id);

            response.sendRedirect("projects?action=view");
        }
        if (action.equalsIgnoreCase("edit")) {
            int id = Integer.parseInt(request.getParameter("id"));
            try {
                request.setAttribute("project", projectdao.getProjectById(id));
                request.setAttribute("users", userdao.getUserlist());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            RequestDispatcher view = request.getRequestDispatcher("project/editProject.jsp");
            view.forward(request, response);
        }
        if (action.equalsIgnoreCase("task")) {
            try {
                request.setAttribute("id", request.getParameter("id"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            RequestDispatcher view = request.getRequestDispatcher("Task/task.jsp");
            view.forward(request, response);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String title = request.getParameter("title");
        int user = Integer.parseInt(request.getParameter("user"));
        int id = Integer.parseInt(request.getParameter("id"));
        int creator = Integer.parseInt(request.getParameter("creator"));
        String update = request.getParameter("update");

        Project project = new Project();
        project.setTitle(title);
        project.setAssigned_user_id(user);
        project.setCreator_id(creator);
        project.setId(id);

        if (update.equalsIgnoreCase("true")) {
            projectdao.updateProject(project);
        } else {

            projectdao.addProject(project);
        }

        response.sendRedirect("projects?action=view");
    }

}
