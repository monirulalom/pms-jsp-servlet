/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.TaskDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Task;

/**
 *
 * @author monirulalom
 */
public class TaskController extends HttpServlet {

    private TaskDao taskdao;

    public TaskController() {
        taskdao = new TaskDao();

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String status = request.getParameter("status");
        taskdao.updateStatus(id, status);
        response.sendRedirect("projects?action=view");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int project_id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");

        Task task = new Task();
        task.setProject_id(project_id);
        task.setStatus("Started");
        task.setTitle(title);
        taskdao.addTask(task);

        response.sendRedirect("projects?action=view");

    }

}
