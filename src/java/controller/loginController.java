package controller;

import dao.UserDao;
import model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "loginController", urlPatterns = {"/login"})
public class loginController extends HttpServlet {

 private UserDao loginDao;

    public void init() {
        loginDao = new UserDao();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = new User();
        user.setUsername(email);
        user.setPassword(password);

        try {
            if (loginDao.login(user)) {
                HttpSession session = request.getSession();
                session.setAttribute("user_id",loginDao.getId(email));
                 session.setAttribute("user_email",email);
                 session.setAttribute("user_role",loginDao.getRole(email));
                response.sendRedirect("projects?action=view");
            } else {
                HttpSession session = request.getSession();
                //session.setAttribute("user", username);
                //response.sendRedirect("login.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
