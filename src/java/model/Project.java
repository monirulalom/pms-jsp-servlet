package model;

import java.util.ArrayList;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import dao.TimeAgo;

public class Project {

    private int id;
    private String title;
    private int creator_id;
    private int assigned_user_id;
    private Date created_at;
    private Date updated_at;
    private ArrayList<Task> task = new ArrayList<>();
    String Status;
    String time_status;
    long interval;
    String ago;
    String create_time;
    String manager;
    String developer;

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getTime_status() {
        return time_status;
    }

    public String getAgo() {
        return ago;
    }

    public void setAgo(String ago) {
        this.ago = ago;
    }

    public void setTime_status(String time_status) {
        this.time_status = time_status;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(int creator_id) {
        this.creator_id = creator_id;
    }

    public int getAssigned_user_id() {
        return assigned_user_id;
    }

    public void setAssigned_user_id(int assigned_user_id) {
        this.assigned_user_id = assigned_user_id;
    }


    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public void setTask(ArrayList<Task> task) {
        this.task = new ArrayList<>(task);
    }
   public ArrayList<Task> getTask(){
    
        return this.task;
    }
       public void setCreated_at(Date d) {
        this.created_at = d;
        this.setInterval();
        this.setAgo();
        this.setStatus();
    }


    public void setInterval() {
        Date d = new Date();
        long t = d.getTime() - created_at.getTime();
        this.interval = t;
    }

    public void setAgo() {
        this.ago = TimeAgo.toDuration(interval);

    }

    public void setStatus() {
        if(this.interval <15000)
            this.Status = "new";
        else if(this.interval<30000)
            this.Status = "old";
        else
            this.Status="expired" ;
    }
    

}
