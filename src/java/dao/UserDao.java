/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.User;

public class UserDao {

    private Connection connection;

    public UserDao() {
        connectionclass con = new connectionclass();
        try {
            connection = con.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<User> getUserlist() throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement("select * from user");
        ArrayList<User> users = new ArrayList();

        ResultSet res = preparedStatement.executeQuery();
        while (res.next()) {
            User user = new User();
            user.setUsername(res.getString("email"));
            user.setRole(res.getString("role"));
            user.setId(Integer.parseInt(res.getString("id")));
            user.setRole(res.getString("role"));
            users.add(user);

        }
        return users;
    }

    public boolean login(User user) throws SQLException {
        boolean status = false;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from user where email = ? and password = ? ");
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getPassword());

            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();

        } catch (SQLException e) {
            // handle exception
        }
        return status;
    }

    public int register(User user) throws SQLException {
        int res = 0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into user(email,password) values(?,?) ");
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getPassword());

            System.out.println(preparedStatement);
            res = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            // handle exception
        }
        return res;
    }

    public int getId(String email) throws SQLException {
        int id=0;
        String query = "select * from user where email = '"+email+"'";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ArrayList<User> users = new ArrayList();

        ResultSet res = preparedStatement.executeQuery();
        while (res.next()) {

            id = Integer.parseInt(res.getString("id"));

        }
        return id;
    }
       public String getRole(String email) throws SQLException {
        String role="developer";
        String query = "select * from user where email = '"+email+"'";
        PreparedStatement preparedStatement = connection.prepareStatement(query);

        ResultSet res = preparedStatement.executeQuery();
        while (res.next()) {

            role = res.getString("role");

        }
        return role;
    }
      public String getEmail(int id) throws SQLException {
        String email="";
        String query = "select * from user where id = '"+id+"'";
        PreparedStatement preparedStatement = connection.prepareStatement(query);

        ResultSet res = preparedStatement.executeQuery();
        while (res.next()) {

            email = res.getString("email");

        }
        return email;
    }
    
    
}
