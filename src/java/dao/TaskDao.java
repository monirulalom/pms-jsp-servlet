package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Project;
import model.Task;

public class TaskDao {

    private Connection connection;

    public TaskDao() {

        connectionclass con = new connectionclass();
        try {
            connection = con.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Task> getTaskList(int id) throws SQLException {

        String query = "select * from task where project_id = " + id;
        ArrayList<Task> tasks = new ArrayList<Task>();
        Statement stmt = connection.createStatement();

        ResultSet res = stmt.executeQuery(query);

        while (res.next()) {
            Task task = new Task();
            task.setId(res.getInt("id"));
            task.setProject_id(res.getInt("project_id"));
            task.setTitle(res.getString("title"));
            task.setStatus(res.getString("status"));
            tasks.add(task);
        }

        return tasks;

    }

    public void addTask(Task task) {
        try {
            String query = "insert into task(title,project_id,status) values ('" + task.getTitle() + "','" + task.getProject_id() + "',+'" + task.getStatus() + "')";

            // String query = "insert into project(title) values('ami')";
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateStatus(int id, String status) {
        try {
            String query = "UPDATE task SET task.status='" + status + "' where id =" + id;
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
