package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Project;
import model.Task;

public class ProjectDao {

    private Connection connection;
    TaskDao taskdao = new TaskDao();
    UserDao userdao = new UserDao();

    public ProjectDao() {

        connectionclass con = new connectionclass();
        try {
            connection = con.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Project> getProjectList() throws SQLException {

        String query = "SELECT * FROM project LEFT join user on project.assigned_user_id = user.id";
        ArrayList<Project> projects = new ArrayList<Project>();
        Statement stmt = connection.createStatement();

        ResultSet res = stmt.executeQuery(query);

        while (res.next()) {
            Project project = new Project();
            project.setId(res.getInt("id"));
            project.setTitle(res.getString("title"));
            project.setCreated_at(res.getTimestamp("created_at"));
            project.setUpdated_at(res.getTimestamp("updated_at"));
            project.setAssigned_user_id(res.getInt("assigned_user_id"));
            project.setCreator_id(res.getInt("creator_id"));
            project.setTask(taskdao.getTaskList(res.getInt("id")));
            project.setDeveloper(userdao.getEmail(res.getInt("assigned_user_id")));
            project.setManager(userdao.getEmail(res.getInt("creator_id")));

            projects.add(project);
        }

        return projects;

    }

    public ArrayList<Project> getProjectListByUser(int id) throws SQLException {

        String query = "select * from project where assigned_user_id =" + id;
        ArrayList<Project> projects = new ArrayList<Project>();
        Statement stmt = connection.createStatement();

        ResultSet res = stmt.executeQuery(query);

        while (res.next()) {
            Project project = new Project();
            project.setId(res.getInt("id"));
            project.setTitle(res.getString("title"));
            project.setCreated_at(res.getTimestamp("created_at"));
            project.setUpdated_at(res.getTimestamp("updated_at"));
            project.setAssigned_user_id(res.getInt("assigned_user_id"));
            project.setCreator_id(res.getInt("creator_id"));
            project.setTask(taskdao.getTaskList(res.getInt("id")));
            project.setDeveloper(userdao.getEmail(res.getInt("assigned_user_id")));
            project.setManager(userdao.getEmail(res.getInt("creator_id")));
            projects.add(project);
        }

        return projects;

    }

    public void addProject(Project project) {
        try {
            String query = "insert into project(title,creator_id,assigned_user_id) values ('" + project.getTitle() + "', '" + project.getCreator_id() + "', '" + project.getAssigned_user_id() + "')";

            // String query = "insert into project(title) values('ami')";
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeProject(int id) {
        try {
            String query = "DELETE FROM project WHERE id=" + id;

            // String query = "insert into project(title) values('ami')";
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Project getProjectById(int id) throws SQLException {
        Project project = new Project();

        String query = "select * from project where id =" + id;

        Statement stmt = connection.createStatement();

        ResultSet res = stmt.executeQuery(query);

        while (res.next()) {

            project.setId(res.getInt("id"));
            project.setTitle(res.getString("title"));
            project.setCreated_at(res.getTimestamp("created_at"));
            project.setUpdated_at(res.getTimestamp("updated_at"));
            project.setAssigned_user_id(res.getInt("assigned_user_id"));
            project.setCreator_id(res.getInt("creator_id"));
            project.setTask(taskdao.getTaskList(res.getInt("id")));
        }

        return project;

    }

    public void updateProject(Project project) {
        String query = "update `project` set `project`.`title`='" + project.getTitle() + "', `project`.`assigned_user_id`='" + project.getAssigned_user_id() + "' where `project`.`id`=" + project.getId();

        System.out.println(query);
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
