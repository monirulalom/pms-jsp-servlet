<%-- 
    Document   : index
    Created on : Jan 7, 2020, 10:57:54 AM
    Author     : monirulalom
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%  if (null != session.getAttribute("user_id")) {
        response.sendRedirect("projects?action=view");
    }%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Project Management System Login</title>
        <link href="bootstrap.css" rel="stylesheet">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="text-center">User login</h1>
            <form action="login" method="POST" class="form-signin">
                <input type="text" name="email" placeholder="Email" class="form-control">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <input  class="btn btn-lg btn-primary btn-block"  type="submit" value="Login">
            </form>
            <p class="text-center">Don't have any account ? <br> <a href="register.jsp" >Create an account</a></p>
        </div>

    </body>
</html>
