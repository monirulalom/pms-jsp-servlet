<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<%  if (null == session.getAttribute("user_id")) {
        response.sendRedirect("unauthorized.jsp");
    }%>

<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <title>Project Management System</title>
        <link href="/jsplogin/bootstrap.css" rel="stylesheet">
        <link href="/jsplogin/style.css" rel="stylesheet">
    </head>
    <body style="display: block">
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="#">Project Management System</a>
            <div class="center"><c:out value="${sessionScope.user_email}" /></div>
            <div class="right"><a class="navbar-brand" href="logout.jsp">Log out</a></div>

        </nav>



        <div class="wrap">

            <h2 class="text-center">Projects </h2>
            <%  String a = (String) session.getAttribute("user_role");
            if (a.equalsIgnoreCase("manager")) { %>
            <a class="btn btn-primary" style="margin:10px" href="projects?action=add">Add Project</a>
            <%}%>

            <section>
                <c:forEach items="${projects}" var="project">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h3 class="card-title">${project.title}</h3>
                            <p class="card-text">Manager: ${project.manager}
                                <br> Developer:  ${project.developer}
                                <br> Created: ${project.ago}</p>
                            <br>Tasks for this project </div>
                                
                        <ul class="list-group list-group-flush">
                            <c:forEach  items="${project.task}" var="task">
                                <li class="list-group-item"><span class="half"><c:out value="${task.title}"></c:out></span><span class="half"> <a title="Mark as completed" class="<c:if test="${task.status == 'completed'}">highlight</c:if>" href="tasks?id=${task.id}&status=completed">Completed</a><a  title="Mark as reviewed" class="<c:if test="${task.status == 'reviewed'}">highlight</c:if>" href="tasks?id=${task.id}&status=reviewed">Reviewed</a></span></li>
                                    </c:forEach>
                        </ul>
                            <% if (a.equalsIgnoreCase("manager")) { %>
                        <div class="card-body">
                            <a href="projects?action=task&id=${project.id}" class="card-link">Add Task</a>
                            <a href="projects?action=edit&id=${project.id}" class="card-link">Edit</a>
                            <a href="projects?action=delete&id=${project.id}" class="card-link">Delete</a>
                        </div>
                            <%} %>
                    </div>
                </c:forEach>
            </section>	
        </div>

    </body>
</html>
<%%>