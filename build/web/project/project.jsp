<%-- 
    Document   : project
    Created on : Mar 8, 2020, 11:20:16 PM
    Author     : monirulalom
--%>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% // if(request.getAttribute("users") == null) response.sendRedirect("facebook.com"); %>
  <%  String a = (String) session.getAttribute("user_role"); if (! a.equalsIgnoreCase("manager")) { response.sendRedirect("unauthorized.jsp");}%>
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <title>Add Project</title>
        <link href="/jsplogin/bootstrap.css" rel="stylesheet">
        <link href="/jsplogin/style.css" rel="stylesheet">
    </head>
    <body>


        <div class="container">
            <h2 class="text-center">Add Project </h2>

            <form action="projects" method="POST" class="form-signin">
                <div class="form-group">
                    <label for="title" >Project Title</label>
                    <input type="text" name="title" class="form-control" placeholder="Project title">
                </div>
                <div class="form-group">
                    <label for="user" >Developer</label>
                    <select name="user" class="form-control">
                        <c:forEach items="${users}" var="user">
                            <option value="${user.id}">${user.email}</option>
                        </c:forEach> 
                    </select>
                </div>
                <input type="hidden" value="<c:out value="${sessionScope.user_id}"/>" name="creator">
                <input type="hidden" value="100" name="id">
                <input type="hidden" value="false" name="update">
                <input  class="btn btn-lg btn-primary btn-block" type="submit" value="Add">
            </form>
    </body>
</html>
